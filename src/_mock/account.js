// ----------------------------------------------------------------------

const account = {
  displayName: 'Aman Gupta',
  email: 'admin@animeUchiha.com',
  photoURL: '/assets/images/avatars/avatar_default.jpg',
};

export default account;
